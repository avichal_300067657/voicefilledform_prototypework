'use strict';

// pull in the required packages.
var sdk = require('microsoft-cognitiveservices-speech-sdk');
var fs = require('fs');

// replace with your own subscription key,
// service region (e.g., "westus"), and
// the name of the file you want to run
// through the speech recognizer.
var subscriptionKey = '925a71c3d9044a72a73f7a4e1777f46f';
var serviceRegion = 'westus'; // e.g., "westus"
var filename = 'male.wav'; // 16000 Hz, Mono

// create the push stream we need for the speech sdk.
var pushStream = sdk.AudioInputStream.createPushStream(
	sdk.AudioStreamFormat.getWaveFormatPCM(8000, 16, 1)
);

// open the file and push it to the push stream.
fs.createReadStream(filename)
	.on('data', function(arrayBuffer) {
		pushStream.write(arrayBuffer.buffer);
	})
	.on('end', function() {
		pushStream.close();
		console.log('Read file ended');
	});

// we are done with the setup
console.log('Now recognizing from: ' + filename);

// now create the audio-config pointing to our stream and
// the speech config specifying the language.
var audioConfig = sdk.AudioConfig.fromStreamInput(pushStream);
var speechConfig = sdk.SpeechConfig.fromSubscription(
	subscriptionKey,
	serviceRegion
);

// setting the recognition language to English.
speechConfig.speechRecognitionLanguage = 'en-IN';

// create the speech recognizer.
var recognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);

// start the recognizer and wait for a result.
recognizer.startContinuousRecognitionAsync(data => {
	console.log('continuous');
	console.log(data);
}, console.log);
recognizer.recognizing = function(s, e) {
	 var str = "(recognizing) Reason: " + sdk.ResultReason[e.result.reason] + " Text: " + e.result.text;
	console.log( str);
};

recognizer.recognized = function(s, e) {
	// Indicates that recognizable speech was not detected, and that recognition is done.
	if (e.result.reason === sdk.ResultReason.NoMatch) {
		var noMatchDetail = sdk.NoMatchDetails.fromResult(e.result);
		console.log("\r\n(recognized)  Reason: " + sdk.ResultReason[e.result.reason] + " NoMatchReason: " + sdk.NoMatchReason[noMatchDetail.reason]);
	} else {
	  console.log("\r\n(recognized)  Reason: " + sdk.ResultReason[e.result.reason] + " Text: " + e.result.text);
	}
	//console.log('At recognized-------------------', s, e);
};

/* recognizer.recognizeOnceAsync(
	function(result) {
		console.log('at result ----------');
		console.log(result);

		recognizer.close();
		recognizer = undefined;
	},
	function(err) {
		console.trace('err - ' + err);

		recognizer.close();
		recognizer = undefined;
	}
);
 */
