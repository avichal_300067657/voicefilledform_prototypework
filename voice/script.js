var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent =
	SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

var phrase =
	'create a coupon of type alliance starting from tomorrow at 8 and ending day after tomorrow';

var spokenWords = document.getElementById('wordsSpoken');
var resetBtn = document.querySelector('.resetBtn');
var startBtn = document.querySelector('.startBtn');
var stopBtn = document.querySelector('.stopBtn');
var paraPhrase = document.querySelector('.paraPhrase');
var highlighted = document.querySelector('.highlighted');
var unhighlighted = document.querySelector('.unhighlighted');
var phraseArr = phrase.split(/[!@#$%^&*(),.?":{}|<> ]/).filter(word => !!word);
console.log(phraseArr);
unhighlighted.innerHTML = phrase;
phrase = phrase.toLowerCase();
var grammar = '#JSGF V1.0; grammar phrase; public <phrase> = ' + phrase + ';';
var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
recognition.lang = 'en-IN';
recognition.continuous = true;
recognition.interimResults = true;
recognition.maxAlternatives = 1;
function start() {
	recognition.start();
}

function reset() {
	recognition.stop();
	spokenWords.innerText = '';
	highlighted.innerText = '';
	unhighlighted.innerText = phrase;
}
function stop() {
	recognition.stop();
}
function matchWithPhrase(str) {
	str = str
		.join(' ')
		.split(' ')
		.filter(word => !!word);
	let { cursoredPhrase, uncursoredPhrase } = moveCursorToIndex(
		findLastCorrectMatch(phraseArr, str)
	);
	highlighted.innerHTML = cursoredPhrase + ' ';
	unhighlighted.innerHTML = uncursoredPhrase;
}
function findLastCorrectMatch(str1, str2) {
	for (i = 0; i < str2.length; i++) {
		if (str1[i].toLowerCase() !== str2[i].toLowerCase()) {
			return i - 1;
		}
	}
	return i;
}
function moveCursorToIndex(index) {
	console.log('index-- ', index);
	var cursoredPhrase = '',
		uncursoredPhrase = phrase;
	if (index > -1) {
		cursoredPhrase = phraseArr.slice(0, index).join(' ');
		uncursoredPhrase = phraseArr.slice(index, phraseArr.length).join(' ');
		console.log('cursor-- ', cursoredPhrase);
		console.log('no cursor-- ', uncursoredPhrase);
	}
	return { cursoredPhrase, uncursoredPhrase };
}
recognition.onresult = function(event) {
	// The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
	// The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
	// It has a getter so it can be accessed like an array
	// The first [0] returns the SpeechRecognitionResult at position 0.
	// Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
	// These also have getters so they can be accessed like arrays.
	// The second [0] returns the SpeechRecognitionAlternative at position 0.
	// We then return the transcript property of the SpeechRecognitionAlternative object
	var resultLen = Object.keys(event.results);
	var completeString = [];
	for (let i = 0; i < resultLen.length; i++) {
		console.log('Result: ',event.results[i][0])
		completeString.push(event.results[i][0].transcript.trim());
	}
	console.log(completeString);
	spokenWords.innerHTML = completeString.join(' ');
	matchWithPhrase(completeString);
	/* var speechResult;
	for (i = 0; i < completeString.length; i++) {}
	phrasePara.innerHTML = speechResult; */

	console.log('Confidence: ' + event.results[0][0].confidence);
};

recognition.onspeechend = function() {
	console.log('in end of speech');
};

recognition.onerror = function(event) {
	console.log('object');
};

recognition.onaudiostart = function(event) {
	//Fired when the user agent has started to capture audio.
	console.log('SpeechRecognition.onaudiostart');
};

recognition.onaudioend = function(event) {
	//Fired when the user agent has finished capturing audio.

	console.log('SpeechRecognition.onaudioend');
};

recognition.onend = function(event) {
	//Fired when the speech recognition service has disconnected.
	console.log('SpeechRecognition.onend');
};

recognition.onnomatch = function(event) {
	//Fired when the speech recognition service returns a final result with no significant recognition. This may involve some degree of recognition, which doesn't meet or exceed the confidence threshold.
	console.log('SpeechRecognition.onnomatch');
};

recognition.onsoundstart = function(event) {
	//Fired when any sound — recognisable speech or not — has been detected.
	console.log('SpeechRecognition.onsoundstart');
};

recognition.onsoundend = function(event) {
	//Fired when any sound — recognisable speech or not — has stopped being detected.
	console.log('SpeechRecognition.onsoundend');
};

recognition.onspeechstart = function(event) {
	//Fired when sound that is recognised by the speech recognition service as speech has been detected.
	console.log('SpeechRecognition.onspeechstart');
};
recognition.onstart = function(event) {
	//Fired when the speech recognition service has begun listening to incoming audio with intent to recognize grammars associated with the current SpeechRecognition.
	console.log('SpeechRecognition.onstart');
};
startBtn.addEventListener('click', start);
resetBtn.addEventListener('click', reset);
stopBtn.addEventListener('click', stop);
