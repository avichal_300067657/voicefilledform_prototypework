const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const axios = require('axios');
const speech = require('@google-cloud/speech');
const port = process.env.PORT || 4001;
const index = require('./src/routes/index');
const ss = require('socket.io-stream');
const client = new speech.SpeechClient();
const app = express();
const Readable = require('stream').Readable;
var wav = require('wav');

const s = new Readable();
s._read = () => {};
app.use(function(req, res, next) {
	next();
});
app.use(index);

const server = http.createServer(app);

const request = {
	config: {
		encoding: 'LINEAR16',
		sampleRateHertz: 8000,
		languageCode: 'en-IN'
	},
	interimResults: true // If you want interim results, set this to true
};

const io = socketIo(server);
io.on('connection', socket => {
	const recognizeStream = client
		.streamingRecognize(request)
		.on('error', console.error)
		.on('response', data => {
			console.log('IN data!!!!!!!!', data);
			// console.log(
			// 	`Transcription: ${data.results[0].alternatives[0].transcript}`
			// );
		})
		.on('data', console.log);
	//socket.pipe(recognizeStream);
	/* ss(socket).on('AudioStream', (stream, data) => {
		//process.stdout.write(stream)
		//	stream.pipe(process.stdout);
		console.log(data);
		//	process.stdout.write(stream + '\n');
		stream.on('data', data => {
			let newData = data;
			let finalData = Int16Array.from(
				newData,
				0,
				Math.floor(newData.byteLength / 2)
			);
			//let buf = new Buffer(newData, 0, newData.byteLength);
			recognizeStream.write(finalData);
			// finalData.map(item => buf.writeInt16LE(item));
			//console.log('HERE!!!!!!', finalData);
		});
		// stream.on('start', console.log);
		// stream.on('end', console.log);
		//	stream.pipe(fileWriter);
	}); */

	socket.on('message', data => {

		console.log('From plain socket', data || '');
		// let dataArr = Object.keys(data).map(function(key) {
		// 	return data[key];
		// });
		// let int16 = dataArr && dataArr.map(n => n * Number.MAX_SAFE_INTEGER);
		// //console.log(Int16Array.from(int16));
		// const buffer = new Int16Array(data, 0, Math.floor(data.byteLength / 2));

		//s.push(Buffer.from(int16));
		// stream2.write(
		// 	Int16Array.from(data.map(n => n * Number.MAX_SAFE_INTEGER))
		// );
		// for (i in data) {
		// 	data[i] && console.log(data[i]);
		// }
		const buffer = new Buffer(Object.keys(data).map(i => data[i]));
		//console.log(Object.keys(data).map(i=>data[i]))
		//console.log(buffer);
		let newD = JSON.parse(data);
		let finalO = Object.keys(newD).map(i => newD[i]);
		//console.log(finalO)
		recognizeStream.write(finalO);
		//stream2.write(buffer);
		//console.log('Audio', buffer);
	});
	//s.pipe(recognizeStream);
});
server.listen(port, () => console.log(`Listening on port ${port}`));

/* 

const fs = require('fs');

const filename = './test1.flac';
// const encoding = 'Encoding of the audio file, e.g. LINEAR16';
// const sampleRateHertz = 16000;
 const languageCode = 'en-US';

const request = {
  config: {
    encoding: 'FLAC',
	sampleRateHertz: '44100',
	languageCode
  },
  interimResults: true, // If you want interim results, set this to true
};

// Stream the audio to the Google Cloud Speech API
const recognizeStream = client
  .streamingRecognize(request)
  .on('error', console.error)
  .on('data', data => {
    console.log(
      `Transcription: ${data.results[0].alternatives[0].transcript}`
    );
  });

// Stream an audio file from disk to the Speech API, e.g. "./resources/audio.raw"
fs.createReadStream(filename).on('data',data=>{
	console.log(data)
	recognizeStream.write(data)
})//.pipe(recognizeStream);

 */
