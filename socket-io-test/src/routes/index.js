const express = require('express');
const path = require('path');
let router = new express.Router();

router.get('/',(req,res)=>{
	res.sendFile(path.join(__dirname,'../pages/index.html'));
})
module.exports=router;